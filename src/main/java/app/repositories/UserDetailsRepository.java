package app.repositories;

import app.models.GmailOAuth2User;
import app.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDetailsRepository extends JpaRepository<GmailOAuth2User, Long> {
}
