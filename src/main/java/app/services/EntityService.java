package app.services;

import java.util.List;
import java.util.Optional;

public interface EntityService<T> {

    public List<T> getAll();
    public Optional<T> get(long id);
    public void add(T entity);
    public void update(long id, T entity);
    public void delete(long id);

}
