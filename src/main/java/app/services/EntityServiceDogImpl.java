package app.services;

import app.models.Dog;
import app.repositories.DogRepository;
import app.services.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EntityServiceDogImpl implements EntityService<Dog> {

    @Autowired
    public DogRepository dogRepository;

    @Override
    public List<Dog> getAll() {
        return dogRepository.findAll();
    }

    @Transactional
    public Optional<Dog> get(long id) {
        return dogRepository.findById(id);
    }

    @Transactional
    public void add(Dog dog) {
        dogRepository.save(dog);
    }

    @Transactional
    public void update(long id ,Dog dog) {
        dogRepository.deleteById(id);
        dogRepository.save(dog);
    }

    @Transactional
    public void delete(long id) {
        dogRepository.deleteById(id);
    }
}
