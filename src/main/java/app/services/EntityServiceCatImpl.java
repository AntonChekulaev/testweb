package app.services;

import app.models.Cat;
import app.repositories.CatRepository;
import app.services.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EntityServiceCatImpl implements EntityService<Cat> {

    @Autowired
    public CatRepository catRepository;

    public List<Cat> getAll() {
        return catRepository.findAll();
    }

    @Transactional
    public Optional<Cat> get(long id) {
        return catRepository.findById(id);
    }

    @Transactional
    public void add(Cat cat) {
        catRepository.save(cat);
    }

    @Transactional
    public void update(long id,Cat cat) {
        catRepository.deleteById(id);
        catRepository.save(cat);
    }

    @Transactional
    public void delete(long id) {
        catRepository.deleteById(id);
    }



}
