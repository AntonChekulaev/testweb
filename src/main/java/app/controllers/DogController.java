package app.controllers;

import app.models.Dog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import app.services.EntityServiceDogImpl;

import java.util.List;
import java.util.Optional;

@Controller
public class DogController {

    @Autowired
    public EntityServiceDogImpl dogService;

    @GetMapping("/dog")
    public ResponseEntity<List<Dog>> getAll() {
        List<Dog> dogs = dogService.getAll();
        return ResponseEntity.ok().body(dogs);
    }

    @PostMapping("/dog")
    public ResponseEntity<?> save(@RequestBody Dog dog) {
        dogService.add(dog);
        return ResponseEntity.ok().body("New Dog has been saved with ID:" + dog.getId());
    }

    @DeleteMapping("/dog/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        dogService.delete(id);
        return ResponseEntity.ok().body("Book has been deleted successfully.");
    }

    @PutMapping("/dog/{id}")
    public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Dog dog) {
        dogService.update(id, dog);
        return ResponseEntity.ok().body("Book has been updated successfully.");
    }

    @GetMapping("dog/{id}")
    public ResponseEntity<Optional<Dog>> get(@PathVariable("id") long id) {
        Optional<Dog> dog = dogService.get(id);
        return ResponseEntity.ok().body(dog);
    }

}
