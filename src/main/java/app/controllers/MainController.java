package app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import app.services.EntityServiceCatImpl;
import app.services.EntityServiceDogImpl;

import java.security.Principal;

@Controller
public class MainController {

    @Autowired
    public EntityServiceDogImpl dogService;

    @Autowired
    public EntityServiceCatImpl catService;

    @GetMapping("/")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("allCats", catService.getAll());
        modelAndView.addObject("allDogs", dogService.getAll());
        return modelAndView;
    }

    @GetMapping("/login/sso")
    @ResponseBody
    public ModelAndView login(Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("user");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        modelAndView.addObject("auth",  auth);
        return modelAndView;
    }

    @GetMapping("/user")
    @ResponseBody
    public ModelAndView user(Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("user");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        modelAndView.addObject("auth",  auth);
        return modelAndView;
    }

    @GetMapping("/error")
    @ResponseBody
    public ModelAndView error() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        return modelAndView;
    }

    @GetMapping("/admin")
    @ResponseBody
    public ModelAndView admin() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin");
        return modelAndView;
    }

}
